#!/usr/bin/env bash

sudo tlmgr install tcolorbox
sudo tlmgr install environ
sudo tlmgr install pdfcol
sudo tlmgr install adjustbox
sudo tlmgr install titling
sudo tlmgr install enumitem
sudo tlmgr install soul

sudo tlmgr install collection-fontsrecommended
