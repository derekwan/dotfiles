from __future__ import annotations

import re  # noqa: F401
from re import (
    DOTALL,  # noqa: F401
    escape,  # noqa: F401
    findall,  # noqa: F401
    fullmatch,  # noqa: F401
    match,  # noqa: F401
    search,  # noqa: F401
)
