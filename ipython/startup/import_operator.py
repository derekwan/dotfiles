from __future__ import annotations

import operator  # noqa: F401
from operator import (
    add,  # noqa: F401
    and_,  # noqa: F401
    attrgetter,  # noqa: F401
    itemgetter,  # noqa: F401
    mul,  # noqa: F401
    or_,  # noqa: F401
    sub,  # noqa: F401
    truediv,  # noqa: F401
)
