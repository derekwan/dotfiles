from __future__ import annotations

from contextlib import suppress

with suppress(ModuleNotFoundError):
    import rich  # noqa: F401
    from rich import (
        inspect,  # noqa: F401
        pretty,  # noqa: F401
        print,  # noqa: F401
    )
    from rich.traceback import install as _install

    _install()
