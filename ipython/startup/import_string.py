from __future__ import annotations

import string  # noqa: F401
from string import (
    ascii_letters,  # noqa: F401
    ascii_lowercase,  # noqa: F401
    ascii_uppercase,  # noqa: F401
)
