from __future__ import annotations

import numbers  # noqa: F401
from numbers import (
    Integral,  # noqa: F401
    Number,  # noqa: F401
    Real,  # noqa: F401
)
