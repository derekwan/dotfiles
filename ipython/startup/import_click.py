from __future__ import annotations

from contextlib import suppress

with suppress(ModuleNotFoundError):
    import click  # noqa: F401
    from click import (
        BOOL,  # noqa: F401
        FLOAT,  # noqa: F401
        INT,  # noqa: F401
        STRING,  # noqa: F401
        UNPROCESSED,  # noqa: F401
        UUID,  # noqa: F401
        Abort,  # noqa: F401
        Argument,  # noqa: F401
        BadArgumentUsage,  # noqa: F401
        BadOptionUsage,  # noqa: F401
        BadParameter,  # noqa: F401
        BaseCommand,  # noqa: F401
        Choice,  # noqa: F401
        ClickException,  # noqa: F401
        Command,  # noqa: F401
        CommandCollection,  # noqa: F401
        Context,  # noqa: F401
        DateTime,  # noqa: F401
        File,  # noqa: F401
        FileError,  # noqa: F401
        FloatRange,  # noqa: F401
        Group,  # noqa: F401
        HelpFormatter,  # noqa: F401
        IntRange,  # noqa: F401
        MissingParameter,  # noqa: F401
        MultiCommand,  # noqa: F401
        NoSuchOption,  # noqa: F401
        Option,  # noqa: F401
        OptionParser,  # noqa: F401
        Parameter,  # noqa: F401
        ParamType,  # noqa: F401
        Path,  # noqa: F401
        Tuple,  # noqa: F401
        UsageError,  # noqa: F401
        argument,  # noqa: F401
        clear,  # noqa: F401
        command,  # noqa: F401
        confirm,  # noqa: F401
        confirmation_option,  # noqa: F401
        core,  # noqa: F401
        decorators,  # noqa: F401
        echo,  # noqa: F401
        echo_via_pager,  # noqa: F401
        edit,  # noqa: F401
        exceptions,  # noqa: F401
        format_filename,  # noqa: F401
        formatting,  # noqa: F401
        get_app_dir,  # noqa: F401
        get_binary_stream,  # noqa: F401
        get_current_context,  # noqa: F401
        get_text_stream,  # noqa: F401
        getchar,  # noqa: F401
        globals,  # noqa: F401
        group,  # noqa: F401
        help_option,  # noqa: F401
        launch,  # noqa: F401
        make_pass_decorator,  # noqa: F401
        open_file,  # noqa: F401
        option,  # noqa: F401
        parser,  # noqa: F401
        pass_context,  # noqa: F401
        pass_obj,  # noqa: F401
        password_option,  # noqa: F401
        pause,  # noqa: F401
        progressbar,  # noqa: F401
        prompt,  # noqa: F401
        secho,  # noqa: F401
        style,  # noqa: F401
        termui,  # noqa: F401
        types,  # noqa: F401
        unstyle,  # noqa: F401
        utils,  # noqa: F401
        version_option,  # noqa: F401
        wrap_text,  # noqa: F401
    )
