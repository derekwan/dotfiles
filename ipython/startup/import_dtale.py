from __future__ import annotations

from contextlib import suppress

with suppress(ModuleNotFoundError):
    import dtale  # noqa: F401
