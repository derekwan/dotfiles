from __future__ import annotations

import types  # noqa: F401
from types import (
    MemberDescriptorType,  # noqa: F401
    MethodDescriptorType,  # noqa: F401
    MethodType,  # noqa: F401
    MethodWrapperType,  # noqa: F401
    ModuleType,  # noqa: F401
    new_class,  # noqa: F401
)
