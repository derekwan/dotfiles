from __future__ import annotations

import functools  # noqa: F401
from functools import (
    cache,  # noqa: F401
    cached_property,  # noqa: F401
    cmp_to_key,  # noqa: F401
    lru_cache,  # noqa: F401
    partial,  # noqa: F401
    partialmethod,  # noqa: F401
    reduce,  # noqa: F401
    singledispatch,  # noqa: F401
    singledispatchmethod,  # noqa: F401
    total_ordering,  # noqa: F401
    update_wrapper,  # noqa: F401
    wraps,  # noqa: F401
)
