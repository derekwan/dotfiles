from __future__ import annotations

import logging  # noqa: F401
from logging import (
    DEBUG,  # noqa: F401
    ERROR,  # noqa: F401
    INFO,  # noqa: F401
    WARNING,  # noqa: F401
    basicConfig,  # noqa: F401
)
from logging.config import (
    dictConfig,  # noqa: F401
)
