from __future__ import annotations

from contextlib import suppress

with suppress(ModuleNotFoundError):
    from bidict import (
        bidict,  # noqa: F401
    )
