from __future__ import annotations

from contextlib import suppress

with suppress(ModuleNotFoundError):
    import more_itertools  # noqa: F401
    from more_itertools import (
        all_equal,  # noqa: F401
        consume,  # noqa: F401
        distribute,  # noqa: F401
        divide,  # noqa: F401
        dotproduct,  # noqa: F401
        filter_except,  # noqa: F401
        first,  # noqa: F401
        first_true,  # noqa: F401
        flatten,  # noqa: F401
        grouper,  # noqa: F401
        interleave,  # noqa: F401
        interleave_longest,  # noqa: F401
        intersperse,  # noqa: F401
        iter_except,  # noqa: F401
        iterate,  # noqa: F401
        last,  # noqa: F401
        lstrip,  # noqa: F401
        map_except,  # noqa: F401
        ncycles,  # noqa: F401
        nth,  # noqa: F401
        nth_combination,  # noqa: F401
        nth_or_last,  # noqa: F401
        only,  # noqa: F401
        padnone,  # noqa: F401
        pairwise,  # noqa: F401
        partition,  # noqa: F401
        powerset,  # noqa: F401
        prepend,  # noqa: F401
        quantify,  # noqa: F401
        random_combination,  # noqa: F401
        random_permutation,  # noqa: F401
        random_product,  # noqa: F401
        repeatfunc,  # noqa: F401
        roundrobin,  # noqa: F401
        rstrip,  # noqa: F401
        split_after,  # noqa: F401
        split_at,  # noqa: F401
        split_before,  # noqa: F401
        split_into,  # noqa: F401
        split_when,  # noqa: F401
        strip,  # noqa: F401
        tail,  # noqa: F401
        unique_everseen,  # noqa: F401
        unique_justseen,  # noqa: F401
        unzip,  # noqa: F401
        windowed,  # noqa: F401
        zip_equal,  # noqa: F401
    )

    try:
        import tabulate as _tabulate  # noqa: F401
    except ModuleNotFoundError:
        from more_itertools import (
            tabulate,  # noqa: F401
        )

    try:
        import utilities as _utilities  # noqa: F401
    except ModuleNotFoundError:
        from more_itertools import (
            always_iterable,  # noqa: F401
            chunked,  # noqa: F401
            one,  # noqa: F401
            take,  # noqa: F401
        )
