from __future__ import annotations

import gzip  # noqa: F401
from gzip import (
    BadGzipFile,  # noqa: F401
    GzipFile,  # noqa: F401
    compress,  # noqa: F401
    decompress,  # noqa: F401
)
