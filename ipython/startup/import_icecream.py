from __future__ import annotations

from contextlib import suppress

with suppress(ModuleNotFoundError):
    import icecream  # noqa: F401
    from icecream import ic  # noqa: F401
