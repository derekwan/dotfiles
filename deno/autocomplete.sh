#!/usr/bin/env bash

if [ -n "${BASH_VERSION+x}" ]; then
	# shellcheck source=/dev/null
	source /usr/local/etc/bash_completion.d/deno.bash
fi
